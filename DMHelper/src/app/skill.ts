export class Skill {
    name: string;
    bonus: number;

    generateSkills(data: object): Skill[]{
        let ret = [];
        const skillList = [
            'athletics',
            'acrobatics',
            'sleight_of_hand',
            'stealth',
            'arcana',
            'history',
            'investigation',
            'nature',
            'religion',
            'animal_handling',
            'insight',
            'medicine',
            'perception',
            'survival',
            'deception',
            'intimidation',
            'performance',
            'persuasion',
        ];
        skillList.forEach(element => {
            if(data[element]){
                let temp = new Skill();
                temp.name = element;
                temp.bonus = data[element];
                ret.push(temp);
            }
        });
        return ret;
    }
}
