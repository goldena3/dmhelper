import { SpellsService } from './../spells.service';
import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { element } from 'protractor';
import { Spell } from '../spell';

@Component({
  selector: 'app-all-spells',
  templateUrl: './all-spells.component.html',
  styleUrls: ['./all-spells.component.css']
})
export class AllSpellsComponent implements OnInit {
  spells: Spell[];
  filtered: Spell[];
  constructor(private spellsService: SpellsService) {
    if(!this.spells){
      this.spells = spellsService.getAllSpells();
      this.filtered = this.spells;
    }
    this.spells = this.spells.sort((a, b) => {
      return a.level - b.level;
    });
    console.log(this.spells);
   }

  ngOnInit() {
    this.spellsService.getSpells();
  }

  getSpell(): Spell{
    return new Spell();
  }
  filterSpells(level, school: string, usedBy: string) {
    console.log(usedBy);
    this.filtered = this.spells.filter(spell => {
      if (
        (!level || spell.level == level) &&
        (!usedBy || this.hasClass(spell.classes, usedBy)) &&
        (!school || spell.school.name === school)
        ) {
        return true;
      }
      return false;
    });
  }

  hasClass(classes, usedBy) {
    console.log(classes)
    for(let i = 0; i < classes.length; i++){
      if(classes[i].name === usedBy){
        return true;
      }
    }
    return false;
  }

  getIndexes(){
    let number = Math.floor(this.filtered.length / 3);
    if(this.filtered.length%3 != 0){
      number++;
    }
    let ret = [];
    let test = 0;
    for(var i = 0; i < number; i++){
      ret.push(test);
      test += 3;
    }
    return ret;
  }
}
