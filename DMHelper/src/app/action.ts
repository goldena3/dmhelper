export class Action {
    attackBonus: number;
    damage_bonus: number;
    damage_dice: string;
    attack_bonus: number;
    desc: string;
    name: string;

    generateActions(data: []) {
        const ret = [];
        data.forEach(element => {
            const temp = new Action();
            temp.attackBonus = element['attack_bonus'];
            temp.desc = element['desc'];
            temp.name = element['name'];
            temp.damage_dice = element['damage_dice'];
            ret.push(temp);
        });
        return ret;
    }
}
