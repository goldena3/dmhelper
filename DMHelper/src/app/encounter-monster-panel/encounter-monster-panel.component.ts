import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-encounter-monster-panel',
  templateUrl: './encounter-monster-panel.component.html',
  styleUrls: ['./encounter-monster-panel.component.css']
})
export class EncounterMonsterPanelComponent implements OnInit {
  @Input() monster;
  constructor() {
  }

  ngOnInit() {
  }

  getIndex(type: string){
    if(this.monster[type]){
      let num = Math.floor(this.monster[type].length / 2);
      if (this.monster[type].length % 2 !== 0) {
        num++;
      }
      const ret = [];
      for (let i = 0; i < num; i += 2){
        ret.push(i);
      }
      return ret;
    }
    return []
  }

  calcMod(score) {
    if (!score) {
      score  = 0;
    }
    let temp = score - 10;
    temp = Math.floor(temp / 2);
    return temp;
  }

}
