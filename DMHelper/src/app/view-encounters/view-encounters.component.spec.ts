import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEncountersComponent } from './view-encounters.component';

describe('ViewEncountersComponent', () => {
  let component: ViewEncountersComponent;
  let fixture: ComponentFixture<ViewEncountersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEncountersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEncountersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
