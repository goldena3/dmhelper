import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Monster } from './monster';

@Injectable()
export class MonstersService {
  monsters: Monster[];
  custMonsters = [];
  getting = false;
  constructor(private client: HttpClient) {
    if(!this.monsters){
      this.monsters = [];
    }
  }

  getMonsters() {
    if (!this.monsters.length && !this.getting) {
      this.getting = true;
      this.client.get('http://www.dnd5eapi.co/api/monsters').subscribe(data => {
        let keys = Object.keys(data);
        keys.forEach(element => {

          if(element === 'results'){
            for(var i = 0; i < data[element].length; i++){
              this.getMonster(data[element][i]['url'])
            }
          }
        });
      });
    }
  }

  getMonster(url: string){
    url = 'http://www.dnd5eapi.co' + url;
    this.client.get(url).subscribe(data => {
      const temp = new Monster();
      temp.create(data);
      this.monsters.push(temp);
    });
  }

  getAllMonsters(){
    return this.monsters;
  }

  getMonsterById(id: string): Monster {
    for(let i = 0; i < this.monsters.length; i++){
      if(id === this.monsters[i]['id']){
        return this.monsters[i];
      }
    }
    return null;
  }

  cloneMonster(id) {
    for (let i = 0; i < this.monsters.length; i++) {
      if (id === this.monsters[i]['id']) {
        return Object.assign({}, this.monsters[i]);
      }
    }
    return null;
  }
}
