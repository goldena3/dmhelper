import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Monster } from './monster';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SaveMonsterService {

  constructor(private client: HttpClient) { }

  SaveMonster(newmonster: Monster) {
    this.client.get(environment.Backend + '/api/SaveMonsterApi.php?data=' + JSON.stringify(newmonster)).subscribe(data => {
      console.log(data);
  });
  }
}


