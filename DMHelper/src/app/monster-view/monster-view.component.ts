import { MonstersService } from './../monsters.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Monster } from '../monster';
import { EncounterService } from '../encounter.service';
import { SaveMonsterService } from '../save-monster.service';


@Component({
  selector: 'app-monster-view',
  templateUrl: './monster-view.component.html',
  styleUrls: ['./monster-view.component.css']
})
export class MonsterViewComponent implements OnInit {
  id: string;
  monster: Monster;
  constructor(private encs: EncounterService,
              private route: ActivatedRoute,
              private monsterService: MonstersService,
              private saveMonster: SaveMonsterService) {
    this.monster =  this.monsterService.getMonsterById(this.id);
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.id = params['id'];
      this.monster = this.monsterService.getMonsterById(this.id);
      if (!this.monster) {
        this.monsterService.getMonsters();
      }
   });
  }

  getIndex(type: string) {
    if(this.monster[type]){
      let num = Math.floor(this.monster[type].length / 2);
      if (this.monster[type].length % 2 !== 0) {
        num++;
      }
      const ret = [];
      for (let i = 0; i < num; i += 2){
        ret.push(i);
      }
      return ret;
    }else{
      return [];
    }
  }

  getEncounternames() {
    return this.encs.getNames();
  }

  addMonster(i, amount) {
    this.encs.addMonster(i, amount, this.id);
  }

  calcMod(score) {
    return Math.floor((score - 10) / 2);
  }

  saveMosnter() {
    this.saveMonster.SaveMonster(this.monster);
  }

}
