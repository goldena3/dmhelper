import { SpellsService } from './spells.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule  } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { MonstersService} from './monsters.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { AllMonstersComponent } from './all-monsters/all-monsters.component';
import { MonsterPanelComponent } from './monster-panel/monster-panel.component';
import { MonsterViewComponent } from './monster-view/monster-view.component';
import { AllSpellsComponent } from './all-spells/all-spells.component';
import { SpellPanelComponent } from './spell-panel/spell-panel.component';
import { SpellViewComponent } from './spell-view/spell-view.component';
import { EncounterService } from './encounter.service';
import { ViewEncountersComponent } from './view-encounters/view-encounters.component';
import { RunEncountersComponent } from './run-encounters/run-encounters.component';
import { EncounterMonsterPanelComponent } from './encounter-monster-panel/encounter-monster-panel.component';
import { CreateMonsterComponent } from './create-monster/create-monster.component';
import { SignUpComponent } from './sign-up/sign-up.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    NotFoundComponent,
    AllMonstersComponent,
    MonsterPanelComponent,
    MonsterViewComponent,
    AllSpellsComponent,
    SpellPanelComponent,
    SpellViewComponent,
    ViewEncountersComponent,
    RunEncountersComponent,
    EncounterMonsterPanelComponent,
    CreateMonsterComponent,
    SignUpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule 
  ],
  providers: [
    MonstersService,
    SpellsService,
    EncounterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
