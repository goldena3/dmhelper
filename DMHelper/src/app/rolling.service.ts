import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RollingService {

  constructor() { }

  roll(seed) {
    return Math.floor((Math.random() * seed) + 1);
  }
}
