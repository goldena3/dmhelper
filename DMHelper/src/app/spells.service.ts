import { Spell } from './spell';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpellsService {
  spells: Spell[];
  constructor(private client: HttpClient) {
    if(!this.spells){
      this.spells = []
    }
   }

  getSpells() {
    if (!this.spells.length) {
      this.client.get('http://www.dnd5eapi.co/api/spells').subscribe(data => {
        let keys = Object.keys(data);
        keys.forEach(element => {
          if(element === 'results') {
            for(var i = 0; i < data[element].length; i++){
              this.getSpell(data[element][i]['url']);
            }
          }
        });
      });
    }
  }

  getAllSpells(): Spell[] {
    this.spells = this.spells.sort((a, b) => {
      return a.level - b.level;
    });
    return this.spells;
  }

  getSpell(url: string) {
    url = 'http://www.dnd5eapi.co' + url;
    this.client.get(url).subscribe(data => {
      const temp = new Spell();
      temp.create(data);
      this.spells.push(temp);
    });
  }

  getSpellById(id) {
    for(let i = 0; i < this.spells.length; i++){
      if(this.spells[i].id === id){
        return this.spells[i];
      }
    }
    return null;
  }
}


