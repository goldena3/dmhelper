import { TestBed } from '@angular/core/testing';

import { SaveMonsterService } from './save-monster.service';

describe('SaveMonsterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveMonsterService = TestBed.get(SaveMonsterService);
    expect(service).toBeTruthy();
  });
});
