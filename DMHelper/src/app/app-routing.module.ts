import { CreateMonsterComponent } from './create-monster/create-monster.component';
import { ViewEncountersComponent } from './view-encounters/view-encounters.component';
import { AllSpellsComponent } from './all-spells/all-spells.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AllMonstersComponent } from './all-monsters/all-monsters.component';
import { MonsterViewComponent } from './monster-view/monster-view.component';
import { SpellViewComponent } from './spell-view/spell-view.component';
import { RunEncountersComponent } from './run-encounters/run-encounters.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'runEncounter/:index', component: RunEncountersComponent},
  {path: 'viewEncounters', component: ViewEncountersComponent},
  {path: 'spellView/:id', component: SpellViewComponent},
  {path: 'monsterView/:id', component: MonsterViewComponent},
  {path: 'spells', component: AllSpellsComponent},
  {path: 'monsters', component: AllMonstersComponent},
  {path: 'creatMonster', component: CreateMonsterComponent},
  {path: 'signUp', component: SignUpComponent},
  {path: '**', component: NotFoundComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
