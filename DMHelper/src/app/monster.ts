import { SavingThrow } from './saving-throw';
import {  Skill } from './skill';
import { Ability } from './ability';
import { Action } from './action';

export class Monster {
    currHealth            : number;
    id                    : string;
    name                  : string;
    size                  : string;
    type                  : string;
    subtype               : string;
    alignment             : string;
    armor_class           : number;
    hit_points            : number;
    hit_dice              : number;
    speed                 : string[];
    strength              : number;
    charisma              : number;
    dexterity             : number;
    constitution          : number;
    intelligence          : number;
    wisdom                : number;
    saves                 : SavingThrow[]
    skills                : Skill[];
    damage_vulnerabilities: string[];
    damage_resistances    : string[];
    damage_immunities     : string[];
    condition_immunities  : string[];
    senses                : string[];
    languages             : string[];
    cr                    : number;
    special_abilities     : Ability[];
    actions               : Action[];
    legendary_actions     : Action[];
    initiative = 0;
    

    create(data: object){
        this.id                     = data['_id'];
        this.name                   = data['name'];
        this.size                   = data['size'];
        this.type                   = data['type'];
        this.subtype                = data['subtype'];
        this.alignment              = data['alignment'];
        this.armor_class            = data['armor_class'];
        this.hit_points             = data['hit_points'];
        this.currHealth             = this.hit_points;
        this.hit_dice               = data['hit_dice'];
       this.speed                   = this.generateSpeeds(data['speed']);
        this.strength               = data['strength'];
        this.dexterity              = data['dexterity'];
        this.charisma               = data['charisma'];
        this.constitution           = data['constitution'];
        this.intelligence           = data['intelligence'];
        this.wisdom                 = data['wisdom'];
        this.damage_vulnerabilities = data['damage_vulnerabilities'];
       this.damage_resistances     = data['damage_resistances'];
        this.damage_immunities      = data['damage_immunities'];
        this.condition_immunities   = this.generateImunities(data['condition_immunities']);
        this.senses                 = data['senses'];
       this.languages              = data['languages'].split(',');
        this.cr                     = data['challenge_rating'];
        this.skills                 = new Skill().generateSkills(data);
        this.saves                  = new SavingThrow().generateThrows(data);
        let temp                        = new Ability();
        if(data['special_abilities'] ){
            this.special_abilities = temp.generateAblilities(data['special_abilities']);
        }
        if(data['actions']){
            this.actions = new Action().generateActions(data['actions']);
        }
        if(data['legendary_actions']){
            this.legendary_actions = new Action().generateActions(data['legendary_actions']);
        }
    }

    generateSpeeds(speeds) {
        const keys = Object.keys(speeds);
        const values = Object.values(speeds);
        const temp = [];
        for(let i = 0; i < keys.length; i++) {
            temp[i] = keys[i] + ': ' + values[i];
        }
        return temp;
    }
    generateImunities(immunities) {
        const temp = [];
        for(let i = 0; i < immunities.length; i++) {
            temp[i] = immunities[i].name;
        }
        return temp;
    }
}
