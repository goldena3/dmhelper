import { MonstersService } from './../monsters.service';
import { Component, OnInit } from '@angular/core';
import { SavingThrow } from '../saving-throw';
import { Monster } from '../monster';
import { Skill } from '../skill';
import { Action } from '../action';
import {Router} from '@angular/router';
import { Ability } from '../ability';

@Component({
  selector: 'app-create-monster',
  templateUrl: './create-monster.component.html',
  styleUrls: ['./create-monster.component.css']
})
export class CreateMonsterComponent implements OnInit {
  str = 0;
  strBonus = 0;
  dex = 0;
  dexBonus = 0;
  con = 0;
  conBonus = 0;
  int = 0;
  intBonus = 0;
  wis = 0;
  wisBonus = 0;
  cha = 0;
  chaBonus = 0;
  speeds = [];
  acBonus = 0;
  Vulnerabilities = [];
  vuns = [];
  damImmunities = [];
  damsIms = [];
  resistances = [];
  ress = [];
  conditionImmunities = [];
  conIms = [];
  skills = [];
  skill = [];
  act = [];
  actions = [];
  dienum = [];
  dieType = [];
  able = [];
  abilities = [];
  leg = [];
  legActions = [];

  constructor(private router: Router, private MonsterSeverivce: MonstersService) { }

  ngOnInit() {
  }

  saveStat(stat, value: number) {
   switch(stat) {
      case 'str':
        this.str = value;
        break;
      case 'strBonus':
        this.strBonus = value;
        break;
      case 'dex':
        this.dex = value;
        break;
      case 'dexBonus':
        this.dexBonus = value;
        break;
      case 'con':
        this.con = value;
        break;
      case 'conBonus':
        this.conBonus = value;
        break;
      case 'int':
        this.int = value;
        break;
      case 'intBonus':
        this.intBonus = value;
        break;
      case 'wis':
        this.wis = value;
        break;
      case 'wisBonus':
        this.wisBonus = value;
        break;
      case 'cha':
        this.cha = value;
        break;
      case 'chaBonus':
        this.chaBonus = value;
        break;
      case 'acBonus':
        this.acBonus = value;
        break;
   }
  }

  getStatMod(stat) {
    switch (stat) {
      case 'str':
        return this.calcMod(this.str);
      case 'dex':
        return this.calcMod(this.dex);
      case 'con':
          return this.calcMod(this.con);
      case 'int':
        return this.calcMod(this.int);
      case 'wis':
        return this.calcMod(this.wis);
        case 'cha':
          return this.calcMod(this.cha);
    }
  }

  getSave(stat) {
    switch (stat) {
      case 'str':
        return this.calcMod(this.str) + (+this.strBonus);
      case 'dex':
        return this.calcMod(this.dex) + (+this.dexBonus);
      case 'con':
          return this.calcMod(this.con) + (+this.conBonus);
      case 'int':
          return this.calcMod(this.int) + (+this.intBonus);
      case 'wis':
          return this.calcMod(this.wis) + (+this.wisBonus);
      case 'cha':
          return this.calcMod(this.cha) + (+this.chaBonus);
    }
  }
  calcMod(stat: number) {
    return Math.floor((stat - 10) / 2);
  }
  saveElement(index, val, type){
    switch(type){
      case 'vun':
        this.vuns[index] = val;
        break;
       case 'damIm':
        this.damsIms[index] = val;
        break;
      case 'res':
        this.ress[index] = val;
        break;
      case 'conIms':
        this.conIms[index] = val;
        break;
      case 'skillName':
        if (this.skill[index]) {
          this.skill[index].name = val;
        } else {
          this.skill[index] = new Skill();
          this.skill[index].name = val;
        }
        break;
      case 'skillBonus':
        if (this.skill[index]) {
          this.skill[index].bonus = val;
        } else {
          this.skill[index] = new Skill();
          this.skill[index].bonus = val;
        }
        break;
      case 'actName':
          if (this.actions[index]) {
            this.actions[index].name = val;
          } else {
            this.actions[index] = new Action();
            this.actions[index].name = val;
          }
          break;
      case 'actAttBonus':
          if (this.actions[index]) {
            this.actions[index].attack_bonus = val;
          } else {
            this.actions[index] = new Action();
            this.actions[index].attack_bonus = val;
          }
          break;
      case 'actDamBonus':
          if (this.actions[index]) {
            this.actions[index].damage_bonus = val;
          } else {
            this.actions[index] = new Action();
            this.actions[index].damage_bonus = val;
          }
          break;
      case 'actDie':
          if (this.actions[index]) {
            this.actions[index].damage_dice = this.dienum[index] +  this.dieType[index];
          } else {
            this.actions[index] = new Action();
            this.actions[index].damage_dice = this.dienum[index] +  this.dieType[index];
          }
          break;
      case 'actDesc':
          if (this.actions[index]) {
            this.actions[index].desc = val;
          } else {
            this.actions[index] = new Action();
            this.actions[index].desc = val;
          }
          break;
      case 'abName':
          if (this.abilities[index]) {
            this.abilities[index].name = val;
          } else {
            this.abilities[index] = new Ability();
            this.abilities[index].name = val;
          }
          break;
      case 'abDesc':
          if (this.abilities[index]) {
            this.abilities[index].desc = val;
          } else {
            this.abilities[index] = new Ability();
            this.abilities[index].desc = val;
          }
          break;
      case 'abBonus':
          if (this.abilities[index]) {
            this.abilities[index].attackBonus = val;
          } else {
            this.abilities[index] = new Ability();
            this.abilities[index].attackBonus = val;
          }
          break;
       case 'legActName':
          if (this.legActions[index]) {
            this.legActions[index].name = val;
          } else {
            this.legActions[index] = new Action();
            this.legActions[index].name = val;
          }
          break;
      case 'legActBonus':
          if (this.legActions[index]) {
            this.legActions[index].attack_bonus = val;
          } else {
            this.legActions[index] = new Action();
            this.legActions[index].attack_bonus = val;
          }
          break;
      case 'legActDamBonus':
          if (this.legActions[index]) {
            this.legActions[index].damage_bonus = val;
          } else {
            this.legActions[index] = new Action();
            this.legActions[index].damage_bonus = val;
          }
          break;
      case 'legActDie':
          if (this.legActions[index]) {
            this.legActions[index].damage_dice = this.dienum[index] +  this.dieType[index];
          } else {
            this.legActions[index] = new Action();
            this.legActions[index].damage_dice = this.dienum[index] +  this.dieType[index];
          }
          break;
      case 'legActDesc':
          if (this.legActions[index]) {
            this.legActions[index].desc = val;
          } else {
            this.legActions[index] = new Action();
            this.legActions[index].desc = val;
          }
          break;
    }
  }

  calcAc() {
    return  this.calcMod(this.dex) + (+this.acBonus);
  }

  createMonster(name, type, init, hp, cr, align, size, speeds) {
    if(!name || !hp) {
      alert("You need a name and hp dumbass");
      return;
    }
    speeds.forEach(speed => {
      if(speed.match(/\d+/g) != 0) {
        this.speeds.push(speed);
      }
    });
    const monster = new Monster();
    monster.name = name;
    monster.strength = this.str;
    monster.dexterity = this.dex;
    monster.charisma = this.cha;
    monster.constitution = this.con;
    monster.intelligence = this.int;
    monster.wisdom = this.wis;
    monster.speed = this.speeds;
    monster.cr = cr;
    monster.armor_class = this.calcAc();
    monster.alignment = align;
    monster.size = size;
    monster.currHealth = +hp;
    monster.hit_points = hp;
    monster.initiative = this.calcMod(this.dex) + (+init);
    monster.type = type;
    monster.damage_vulnerabilities = this.vuns;
    monster.damage_immunities = this.damsIms;
    monster.damage_resistances = this.ress;
    monster.condition_immunities = this.conIms;
    monster.skills = this.skill;
    monster.actions = this.actions;
    monster.special_abilities = this.abilities;
    monster.legendary_actions = this.legActions;

    const throws = new SavingThrow().generateThrows({
      'constitution_save': this.getSave('con'),
      'intelligence_save': this.getSave('int'),
      'wisdom_save': this.getSave('wis'),
      'strength_save': this.getSave('str'),
      'dexterity_save': this.getSave('dex'),
      'charisma_save': this.getSave('cha')
    });
    monster.saves = throws;
    const len = this.MonsterSeverivce.custMonsters.length;
    monster.id = 'cust' + len;
    this.MonsterSeverivce.custMonsters.push('');
    this.MonsterSeverivce.monsters.push(monster);
    this.router.navigate(['monsterView/' + monster.id]);
  }

}
