import { MonstersService } from './../monsters.service';
import { Monster } from './../monster';
import { Component, OnInit } from '@angular/core';
import { EncounterService } from '../encounter.service';
import { ActivatedRoute } from '@angular/router';
import { Enounter } from '../enounter';
import { RollingService } from '../rolling.service';

@Component({
  selector: 'app-run-encounters',
  templateUrl: './run-encounters.component.html',
  styleUrls: ['./run-encounters.component.css']
})
export class RunEncountersComponent implements OnInit {
  enc: Enounter;
  order = [];
  myMonsters: Monster[];
  curr = 0;
  monsterPanel = false;
  target: number;
  constructor(private roll: RollingService, private encs: EncounterService, private route: ActivatedRoute, private ms: MonstersService) { 
    this.route.params.subscribe( params => {
      this.enc = encs.getEncounterClone(params['index']);
      this.myMonsters = [];
      this.enc.monsters.forEach(monsterId => {
        this.myMonsters.push(ms.cloneMonster(monsterId));
      });
    });
  }

  ngOnInit() {
    document.getElementById('setInitiative').click();
  }

  next() {
    this.curr = (this.curr + 1) % this.order.length;
    this.monsterPanel = this.order[this.curr].id;
  }

  rollInit(dex, i) {
    if (!dex) {
      dex = -5;
    } else {
      dex = dex - 10;
      dex = Math.floor(dex / 2);
    }
    dex += this.roll.roll(20);
    this.setMonsterInitiative(i, dex);
    return dex;
  }

  calcMod(dex) {
    if (!dex) {
      dex = -5;
    } else {
      dex = dex - 10;
      dex = Math.floor(dex / 2);
    }
    return dex;
  }

  createOrder() {
    const total = this.myMonsters.length + this.enc.players.length;
    let found = 0;
    let search = 1;
    this.order = [];
    while (found < total) {
      for(let i = 0; i < this.enc.players.length; i++){
        if (this.enc.players[i].initcitive == search) {
          this.order.unshift(this.enc.players[i]);
          found++;
        }
      }
      for(let i = 0; i < this.myMonsters.length; i++){
        if (this.myMonsters[i].initiative == search) {
          this.order.unshift(this.myMonsters[i]);
          found++;
        }
      }
      search++;
    }
    this.monsterPanel = this.order[this.curr].id;
  }

  setPlayerInitiative(playerIndex, initiative) {
    this.enc.players[playerIndex].initcitive = initiative;
  }

  setMonsterInitiative(monsterIndex, initiative) {
    this.myMonsters[monsterIndex].initiative = initiative;
  }

  adjust(value, mod){
    this.order[this.target].currHealth += (value * mod);
    if (this.order[this.target].currHealth > this.order[this.target].hit_points) {
      this.order[this.target].currHealth = this.order[this.target].hit_points;
    }

    if (this.order[this.target].currHealth <= 0) {
      this.order.splice(this.target, 1);
    }
  }

}
