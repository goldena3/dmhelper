import { SpellsService } from './../spells.service';
import { MonstersService } from './../monsters.service';
import { Component, OnInit, ɵConsole } from '@angular/core';
import { EncounterService } from '../encounter.service';
import { RollingService } from '../rolling.service';
import { $ } from 'protractor';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  numPlayers = 1;
  names = [];
  die = [];
  numTypes = 1;
  result;
  loggedIn = false;
  constructor(private ens: EncounterService, private mService: MonstersService, private sService: SpellsService, private dice: RollingService) { }

  ngOnInit() {
    this.mService.getMonsters();
    this.sService.getSpells();
    this.buildNumOfDies(1);
  }

  saveEncounter(name, note) {
    const players = [];
    for (let i = 0; i < 8; i++){
      if (this.names[i]) {
        players.push(this.names[i]);
      }
    }
    this.ens.addEncounter(players, name, note);
  }

  getNums() {
    return [1, 2, 3 , 4, 5, 6, 7, 8];
  }

  updatePlayerNumber(numPlayers) {
    this.numPlayers = numPlayers;
  }

  getPlayNumbers() {
    const numbers = [];
    for (let i = 0; i < this.numPlayers; i++) {
      numbers.push(i);
    }
    return numbers;
  }

  saveName(num, name) {
    this.names[num] = name;
  }

  buildNumOfDies(num) {
    this.numTypes = num;
    if(num > this.die.length){
      for(let i = this.die.length; i < num; i++) {
        this.die.push({'amount' : 1, 'type' : 4});
      }
    }else{
      this.die = this.die.slice(0,num);
    }
    console.log(this.die)
  }

  getNumder() {
    const numbers = [];
    for (let i = 0; i < this.numTypes; i++) {
      numbers.push(i);
    }
    return numbers;
  }

  changeAmountDie(num, index) {
    this.die[index].amount = num;
  }

  changeTypeDie(type, index) {
    this.die[index]['type'] = type;
  }

  roll() {
    let temp = 0;

    for(let i = 0; i < this.die.length; i++) {
      for ( let j = 0; j < this.die[i].amount; j++) {
        console.log('rolling a d' + this.die[i].type)
        temp += this.dice.roll(this.die[i].type);
      }
    }
    this.result = temp;
  }

  resetResult() {
    this.result = 0;
  }
  resetDiceRoller(){
    this.result = '';
    this.numTypes = 0;
    // this.numTypes = 1;
    console.log(this.numTypes)
    this.die = [];
    (<HTMLInputElement>document.getElementById("numTypesDie")).value = "1";
    (<HTMLInputElement>document.getElementById("num-0")).value = "1";
    (<HTMLInputElement>document.getElementById("type-0")).value = "4";
    this.buildNumOfDies(1);
  }

}
