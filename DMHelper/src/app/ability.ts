import { TmplAstTemplate } from '@angular/compiler';

export class Ability {
    attackBonus: 0;
    desc: string;
    name: string;

    generateAblilities(data: []){
        const ret = [];
        data.forEach(element => {
            const temp = new Ability();
            temp.attackBonus = element['attack_bonus'];
            temp.desc = element['desc'];
            temp.name = element['name'];
            ret.push(temp);
        });
        return ret;
    }
}
