import { Component, OnInit, Input } from '@angular/core';
import { Monster } from '../monster';
@Component({
  selector: 'monster-panel',
  templateUrl: './monster-panel.component.html',
  styleUrls: ['./monster-panel.component.css']
})
export class MonsterPanelComponent implements OnInit {
  @Input() monster: Monster;
  constructor() { 
    
  }

  ngOnInit() {
    
  }

}
