import { Injectable } from '@angular/core';
import { Enounter } from './enounter';

@Injectable({
  providedIn: 'root'
})
export class EncounterService {
  encounters: Enounter[];
  constructor() {
    if (!this.encounters) {
      this.encounters = [];
    }
  }

  addEncounter(players, name, note) {
    const enc = new Enounter(name);
    enc.addPlayers(players);
    enc.note = note;
    this.encounters.push(enc);
    console.log(this.encounters);
  }

  getNames() {
    const names = [];
    for (let i = 0; i < this.encounters.length; i++) {
      names.push(this.encounters[i].name);
    }
    return names;
  }

  addMonster(i, amount, id){
    for(let j = 0; j < amount; j++){
      this.encounters[i].monsters.push(id);
    }
    console.log(this.encounters);
  }

  getEncounterClone(index) {
    return Object.assign({}, this.encounters[index]);
  }
}
