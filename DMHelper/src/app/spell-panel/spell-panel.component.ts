import { Spell } from './../spell';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'spell-panel',
  templateUrl: './spell-panel.component.html',
  styleUrls: ['./spell-panel.component.css']
})
export class SpellPanelComponent implements OnInit {
  @Input() spell: Spell;
  constructor() { }

  ngOnInit() {
  }

}
