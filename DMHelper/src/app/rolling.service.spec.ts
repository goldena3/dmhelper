import { TestBed } from '@angular/core/testing';

import { RollingService } from './rolling.service';

describe('RollingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RollingService = TestBed.get(RollingService);
    expect(service).toBeTruthy();
  });
});
