## Setup
1. Clone repository
2. Open a terminal and navigate to the DMHelper folder
3. Run `npm install`
4. Run `npm install --save jquery`
5. Run `npm install -D @types/jquery`
6. Run `npm install bootstrap`
7. Run`npm install bootstrap jquery popper`

## Run App
1. Run `ng serve` and navagate to 'http://localhost:4200/'
2. Run `ng serve -o` this will open the site in your browser

## Description
An app to allow dms to create and manage encounters. You can add monsters to encounts then use those encounters to roll inititive for your monsters and manage their health. 
Monster data is pulled from the D&D 5e API found here: http://www.dnd5eapi.co/